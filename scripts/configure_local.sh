#!/bin/sh
#
# NAME: CONFIG ASSISTANT


echo "Installing required python packages..."

if ! which pip3 > /dev/null; then
    echo -e "pip3 not found! install it? (Y/n) \c"
    read
    if [ "$REPLY" = "n" ]; then
        exit 0
    fi
    sudo apt install python3-pip -y
fi

pip3 install loguru wikipedia wikiquote

echo "Installed required python packages succesfully."


echo "Installing jenny local abilities and default status variables..."

if [ ! -d ./local ]; then
    echo "error: Local folder not found, did you run the script from the correct directory?"
    exit 1
fi

echo "Python Path [/home/pi/.local/lib/python3.6/site-packages]: \c"
read
if [ -z "${REPLY}" ]; then
    py_packages_path=/home/pi/.local/lib/python3.6/site-packages
else
    py_packages_path=${REPLY}
fi

if [ ! -d "${py_packages_path}/jenny" ]; then
    echo "error: Jenny SDK directory not found, install the sdk first."
    exit 1
fi
mkdir "${py_packages_path}/jenny/local"
if [ ! -d "${py_packages_path}/jenny/local" ]; then
    echo "error: Creating Jenny.Local SDK directory failed."
    exit 1
fi
mkdir "${py_packages_path}/jenny/db"
if [ ! -d "${py_packages_path}/jenny/db" ]; then
    echo "error: Creating Jenny.DB SDK directory failed."
    exit 1
fi

cp -r ./local/abilities/* "${py_packages_path}/jenny/local/"
cp -r ./local/db/* "${py_packages_path}/jenny/db/"
cp -r ./local/starter.py "${py_packages_path}/jenny/starter.py"
echo "Installed Jenny local abilities and default status variables succesfully."


echo "Installing and configuring Mosquitto MQTT SSL/TLS server..."

mosquitto_dest=/etc/mosquitto

sudo apt install mosquitto -y

# openssl genrsa -out ./scripts/ca_certificates/ca.key 2048
# openssl req -new -x509 -key ./scripts/ca_certificates/ca.key -out ./scripts/ca_certificates/ca.crt -days 3650 -subj '/CN=authority'

# openssl genrsa -out ./scripts/certs/server.key 2048
# openssl req -new -out ./scripts/certs/server.csr -key ./scripts/certs/server.key  -days 3650 -subj '/CN=jennylocalserver'
# openssl x509 -req -in ./scripts/certs/server.csr -CA ./scripts/ca_certificates/ca.crt -CAkey ./scripts/ca_certificates/ca.key -CAcreateserial -out ./scripts/certs/server.crt -days 3650

# cp ./scripts/ca_certificates/ca.crt "${py_packages_path}/jenny/protocol/certs/ca.crt"
# cp ./scripts/certs/server.key "${py_packages_path}/jenny/protocol/certs/client.key"
# cp ./scripts/certs/server.crt "${py_packages_path}/jenny/protocol/certs/client.crt"

sudo cp -r ./scripts/mosquitto/* $mosquitto_dest/
# sudo cp -r ./scripts/ca_certificates $mosquitto_dest/
# sudo cp -r ./scripts/certs $mosquitto_dest/

echo "Installed and configured Mosquitto MQTT SSL/TLS server succesfully."


exit 0

