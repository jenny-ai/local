<p align="center">
  <h1 align="center"> Hotel Guest Smart Assistant Box Local Hotel Server</h1>
</p>

<p align="center">
  <b>A local server</b> that <i>every hotel smart assistant uses</i>.
</p>

---

## Table of Contents

- [Table of Contents](#table-of-contents)
- [Description](#description)
- [Abilities](#abilities)

---

## Description

A local server to facilitate an optimal service using local content delivery technology.
<br>

## Abilities
* [x] Weather-forecasting.
* [x] Cities history.
* [x] Quotes.
* [x] Jokes.

<br>
