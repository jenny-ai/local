import wikiquote

def get_quote():
	"""
	 get a quote of the day from wikiquote
		:return tuple of two strings: the quote's content and the the quote's sayer
	"""
	return wikiquote.quote_of_the_day()
	
	
	
if __name__ == '__main__':
	print(get_quote())