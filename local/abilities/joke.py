import requests,json


def get_joke():
	"""
     get a joke from dadjoke website
        :return a string represent the joke content
    """
	request='https://icanhazdadjoke.com/slack'
	r = requests.get(request)
	data = json.loads(r.text)
	return data['attachments'][0]['text']
	
	
	
def jokes_generator():
	"""
     jokes generator 
        :yield a string represent the joke content each time
    """
	while 1:
		yield get_joke()
		
if __name__ == '__main__':
	jokes=jokes_generator()
	print(next(jokes))
	print(next(jokes))